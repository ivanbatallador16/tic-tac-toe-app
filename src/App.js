
import './App.css';

//Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

//React Router DOM
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

//Pages
import Home from './pages/Home';
import Game from './pages/Game';

function App() {


  return (
    <Router>
      <Container fluid className = "landingBackground">
        <Routes>
           <Route path="/" element={<Home/>} />
           <Route path="/game" element={<Game/>} />
        </Routes>
      </Container>
    </Router>

  )
}

export default App;
