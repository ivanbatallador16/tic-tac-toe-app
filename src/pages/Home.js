import { Row, Col, Container } from 'react-bootstrap';

import Landing from '../components/Landing';


export default function Home () {

	return (

		<Container fluid>
			<Row>
				<Col>
					<Landing />
				</Col>
			</Row>
		</Container>
	)
}