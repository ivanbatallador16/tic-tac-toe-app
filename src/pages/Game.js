import { Row, Col, Container} from 'react-bootstrap';
import { useState } from 'react';

import Players from '../components/Players';
import TicTacToe from '../components/TicTacToe';


export default function Game() {

	// Modal State Player X
	const [showPlayerX, setShowPlayerX] = useState(true);
	const handleClosePlayerX = () => setShowPlayerX(false);

	// Modal State Player O
	const [showPlayerO, setShowPlayerO] = useState(false);
    const handleShowPlayerO = () => {setShowPlayerX(false); setShowPlayerO(true);}
    const handleClosePlayerO = () => setShowPlayerO(false);

	return (

		<>
	      <Players
	        showPlayerX={showPlayerX}
	        handleClosePlayerX={handleClosePlayerX}
	        handleShowPlayerO={handleShowPlayerO}
	        showPlayerO={showPlayerO}
	        handleClosePlayerO={handleClosePlayerO}
	      />

	      <Container>
	      	<Row>
	      		<Col>
		            <TicTacToe /> 
		        </Col>
	      	</Row>
	      </Container>
	    </>

	)
}