import './Landing.css';

import React, { useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import GameHistory from './GameHistory';

export default function Landing() {

    

    //Modal State History
    const [showHistory, setShowHistory] = useState(false);
    const handleCloseHistory = () => setShowHistory(false);
    const handleShowHistory = () => setShowHistory(true);

    return (

        <Container fluid>
            <Row>
                <Col className="mt-5 p-3 text-center">
                    <h1 className="landingTitle">TIC-TAC-TOE</h1>
                </Col>
            </Row>
            <Row>
                <Col className="m-2 p-3 text-center">
                    <Link className="homeButton" to="/game">Start New Game!</Link>
                </Col>
            </Row>
            <Row>
                <Col className="m-2 p-3 text-center">
                    <Link className="homeButton" onClick={handleShowHistory} >Game History</Link>
                </Col>
                <GameHistory showHistory={showHistory} handleCloseHistory={handleCloseHistory} />
            </Row>
        </Container>
    );
}
