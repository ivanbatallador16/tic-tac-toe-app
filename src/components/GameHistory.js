import React from 'react';
import { useState, useEffect } from 'react';
import { Modal, Button, Table } from 'react-bootstrap';

export default function GameHistory({ showHistory, handleCloseHistory }) {

    const [data, setGameData] = useState([])

    useEffect(() => {
        if (showHistory) {
            fetchData();
        }
    }, [showHistory]);


    const fetchData = () => {
        
        fetch(`https://tic-tac-toe-mlss.onrender.com/gameHistory/`)
        .then(res => res.json())
        .then(data => {

            setGameData(data)
        })
    }

    const calculateWinner = (game) => {
        const playerXPoints = game.playerXWin * 2 + game.numberOfDraw;
        const playerOPoints = game.playerOWin * 2 + game.numberOfDraw;

        if (playerXPoints > playerOPoints) {
            return game.playerXName;
        } else if (playerOPoints > playerXPoints) {
            return game.playerOName;
        } else {
            return 'Draw';
        }
    }
	
    return (

        <Modal className="modal-game-history" size="lg" show={showHistory} onHide={handleCloseHistory}>
            <Modal.Header closeButton>
                <Modal.Title>Game History</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table bordered hover>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Player X</th>
                            <th>Player O</th>
                            <th>Player X Win</th>
                            <th>Player O Win</th>
                            <th>Draws</th>
                            <th>Winner</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((game, index) => (
                            <tr key={index}>
                                <td>{new Date(game.timeAndDate).toLocaleDateString()}</td>
                                <td>{game.playerXName}</td>
                                <td>{game.playerOName}</td>
                                <td>{game.playerXWin}</td>
                                <td>{game.playerOWin}</td>
                                <td>{game.numberOfDraw}</td>
                                <td>{calculateWinner(game)}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <button variant="secondary" onClick={handleCloseHistory}>
                    <span>Close</span>
                </button>
            </Modal.Footer>
        </Modal>
    );
}
