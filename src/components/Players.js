import './Modal.css';
import TicTacToe from './TicTacToe';


import { useState } from 'react';
import React from 'react';
import { Modal, Form } from 'react-bootstrap';

export default function Players({ showPlayerX, handleClosePlayerX, handleShowPlayerO, showPlayerO, handleClosePlayerO }) {

  const [playerXName, setPlayerXName] = useState("");
  const [playerOName, setPlayerOName] = useState("");

  const handlePlayerNameInputX = (name) => {
      setPlayerXName(name);
  }

  const handlePlayerNameInputO = (name) => {
      setPlayerOName(name);
  }


  return (
    <>
      <TicTacToe player1Name={playerXName} player2Name={playerOName} />

      <Modal
        show={showPlayerX}
        onHide={handleClosePlayerX}
        backdrop="static"
        keyboard={false}
        centered
        className="modal"
      >
        <Modal.Header className="justify-content-center">
          <Modal.Title>Player X</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Please Enter your Name</Form.Label>
              <Form.Control type="text" autoFocus required onChange={(e) => handlePlayerNameInputX(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="justify-content-center">
          <button onClick={handleShowPlayerO}>
            <span>Next</span>
          </button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showPlayerO}
        onHide={handleClosePlayerO}
        backdrop="static"
        keyboard={false}
        centered
        className="modal"
      >
        <Modal.Header className="justify-content-center">
          <Modal.Title>Player O</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
              <Form.Label>Please Enter your Name</Form.Label>
              <Form.Control type="text" autoFocus onChange={(e) => handlePlayerNameInputO(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="justify-content-center">
          <button onClick={handleClosePlayerO}>
            <span>Start Game!</span>
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
