import { Modal, Button } from 'react-bootstrap';

export default function Winner({winner, show, handleClose, boardReset, player1Name, player2Name, saveGame}) {

	const handleStopButtonClick = () => {
	    saveGame();
	    handleClose();
	}

	return (

		<div>
		<Modal 
			show={show}
			backdrop="static"
			keyboard={false}
			centered
			onHide={handleClose}
		>
	        <Modal.Header className = "justify-content-center">
	          <Modal.Title>{winner === "Draw" ? "Draw" : "Winner"}</Modal.Title>
	        </Modal.Header>
	        <Modal.Body className = "text-center"> {winner === "Draw" ? "It's a draw!" : `Player ${winner === "X" ? player1Name : player2Name} wins!`}</Modal.Body>
	        <Modal.Footer className = "justify-content-center">
	        	<button onClick={() => {
	        	  handleClose();
	        	  boardReset();
	        	}}><span>Continue</span></button>
	          	<a href="/" style={{ textDecoration: 'none' }}>
	  	            <button onClick={handleStopButtonClick}>
	  	              <span>Stop</span>
	  	            </button>
	  	          </a>
	        </Modal.Footer>
	      </Modal>



		</div>

	)
}