import './TicTacToe.css';
import { useState, useEffect } from 'react';
import Board from './Board';
import Winner from './Winner';

const Player1 = "X";
const Player2 = "O";

const winningPatterns = [

  //Rows
    {pattern: [0,1,2]},
    {pattern: [3,4,5]},
    {pattern: [6,7,8]},
  //Columns
    {pattern: [0,3,6]},
    {pattern: [1,4,7]},
    {pattern: [2,5,8]},
  //Diagonals
    {pattern: [0,4,8]},
    {pattern: [2,4,6]}

];



export default function TicTacToe ({player1Name, player2Name}) {

   const [tiles, setTiles] = useState(Array(9).fill(null));
   const [playerTurn, setPlayerTurn] = useState(Player1);
   const [winner, setWinner] = useState(null)

   const [player1Wins, setPlayer1Wins] = useState(0);
   const [player2Wins, setPlayer2Wins] = useState(0);
   const [draws, setDraws] = useState(0);

   const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //checking the winner
   function evaluateWinner(tiles){

      let remainingMoves = false;
          for (let tile of tiles) {
              if (tile === null) {
                  remainingMoves = true;
                  break;
              }
          }

          if (!remainingMoves) {
              setWinner("Draw");
              setDraws(draws => draws + 1);
              setShow(true);
              return;
          }

     for (let {pattern} of winningPatterns){
       const tileValue1 = tiles[pattern[0]];
       const tileValue2 = tiles[pattern[1]];
       const tileValue3 = tiles[pattern[2]];

       if (tileValue1 !== null && tileValue1 === tileValue2 && tileValue1 === tileValue3){
         
         setWinner(tileValue1);
         setShow(true)
         if (tileValue1 === Player1) {
            setPlayer1Wins(player1Wins => player1Wins + 1); 
         } else {
            setPlayer2Wins(player2Wins => player2Wins + 1);
         }

       }
     }
   }

   //resetting board
   const boardReset = () => {

      setTiles(Array(9).fill(null));
      setPlayerTurn(Player1);
   }

   const tileClick = (e) => {

    if (tiles[e] !== null){
      return;
    }

    const newBoard = [].concat(tiles);
    newBoard[e] = playerTurn;
    setTiles(newBoard);

    if (playerTurn === Player1){
      setPlayerTurn(Player2);
    }
    else {
      setPlayerTurn(Player1);
    }
   }

   useEffect(() => {
      evaluateWinner(tiles);
   }, [tiles]);


   function saveGame(){

    fetch(`https://tic-tac-toe-mlss.onrender.com/gameHistory/gameHistory`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({

        playerXName: player1Name,
        playerOName: player2Name,
        playerXWin: player1Wins,
        playerOWin: player2Wins,
        numberOfDraw: draws
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
    })
    }
   

  return (

      <div className = "game">
            <h1 className = "m-2">Tic-Tac-Toe</h1>
            <Board tiles={tiles} onTileClick = {tileClick}/>
            <Winner 
              winner={winner} 
              show={show} 
              handleClose={handleClose} 
              boardReset={boardReset} 
              player1Name={player1Name} 
              player2Name={player2Name}
              saveGame={saveGame}
            />

            <div className = "m-2">
                <p>Player {player1Name} (X) Wins: {player1Wins}</p>
                <p>Player {player2Name} (O) Wins: {player2Wins}</p>
                <p>Draws: {draws}</p>
            </div>
      </div>


  )
}